--------------
*** DEPLOYMENT
1.  Use files from /bin directory which were build for Linux amd64 OR
    compile repa-cli and repa-server under your OS:
    > go install repa-cli
    > go install repa-server

2.  Run server: ./repa-server
    it is possible to set the port on which server will start (example: ./repa-server -p=4444)
    default port is 3377

3.  Run client: $ ./repa-cli
    it is possible to set the ip and port to connect (example: ./repa-server -p=4444 -ip=127.0.0.1)
    default values are 127.0.0.1 and 3377


------------------------
*** PROTOCOL DESCRIPTION
    Common request/response structure is:

    header: value[\r\n]
    header: value[\r\n]
    ...
    header: value[\r\n]
    [\r\n]


    Possible request headers: "command", "key", "field", "value".
    Possible response headers: "result", "error".


----------------------------
*** TELNET REQUESTS EXAMPLES
    1. Set string value:
        command: set
        key: user
        value: Andy

    2. Push value to list:
        command: push
        key: ids
        value: 5

    3. Get all keys:
        command: keys

    4. Set value to dictionary:
        command: hset
        key: countries
        field: US
        value: United States

    5. Set expiration of key "ids" in 60 seconds:
        command: expire
        key: ids
        value: 60


    * LIST OF POSSIBLE COMMANDS
      "auth":     authenticate to the server
      "set":      set the string value (if key is new then server return 1 else 0 )
    	"get":      get string value by key
    	"del":      remove item by key (if key was removed then server return 1 else 0 )
    	"keys":     get all keys
    	"push":     push value to list (return number of items in the list)
    	"pop":      pop value from list
    	"hset":     set value of field to dictionary (if field is new then server return 1 else 0 )
    	"hget":     get value of field from dictionary
    	"expire":   set key's expiration (in seconds)
    	"ttl":      get number of seconds in which key will expire


----------------------------
*** EXAMPLES OF USING "repa-cli" CLIENT
    127.0.0.1:3377> set user Andy Conor
    127.0.0.1:3377> get user
    127.0.0.1:3377> del user
    127.0.0.1:3377> keys
    127.0.0.1:3377> push ids 5
    127.0.0.1:3377> pop ids
    127.0.0.1:3377> hset countries US United States
    127.0.0.1:3377> hget countries US
    127.0.0.1:3377> expire countries 60
    127.0.0.1:3377> ttl countries


--------------------------
*** PERSISTENCE TO DISK/DB
    Every 1 minute memory cache is saved to file ./repa.rdb
    You can set some values and stop the server, then
    start server and get some value or keys. Everything will be in place.


-----------
*** SCALING (Master-Slave)
    If you run "repa-server" without "master" parameter then it will be master instance.
    If you specify "master" parameter then it will be slave instance (example: ./repa-server -master=127.0.0.1:3377)

    If you run master and slave instances on the one computer then specify different ports for them.
    For example,
        Start master on default port 3377:
        ./repa-server
        And slave on port 3388:
        ./repa-server -master=127.0.0.1:3377 -p=3388

    And also run servers from different folders,
    because server create "repa.rdb" (persistence file) in the same folder from which run.

    Slave server must be used only for READING.
    Every 30 seconds slave server send "sync" request to master and get the master data.


------------------
*** AUTHENTICATION
    Starting server you can specify the password of server (example: ./repa-server -pwd 123).
    If password is specified then all clients must go through authentication process before execute any command
    For example (using "repa-client"): auth 123



---------------------
*** PERFORMANCE TESTS

    === When a client connection to the server ===
    ----------------------------------------------
    BenchmarkServerSet-4    	   50000	     29280 ns/op
    BenchmarkServerGet-4    	   50000	     28544 ns/op
    BenchmarkServerKeys-4   	   50000	     28042 ns/op
    BenchmarkServerPush-4   	   50000	     32588 ns/op
    BenchmarkServerPop-4    	   50000	     31454 ns/op
    BenchmarkServerHset-4   	   50000	     33455 ns/op
    BenchmarkServerHget-4   	   50000	     32988 ns/op
    BenchmarkServerExpire-4 	   50000	     32731 ns/op
    BenchmarkServerTtl-4    	   50000	     31709 ns/op

    === Requests directly to memory storage ===
    -------------------------------------------
    BenchmarkStorageSet-4   	 3000000	       615 ns/op
    BenchmarkStorageGet-4   	 5000000	       249 ns/op
    BenchmarkStorageKeys-4  	 3000000	       454 ns/op
    BenchmarkStoragePush-4  	 1000000	      1232 ns/op
    BenchmarkStoragePop-4   	 5000000	       332 ns/op
    BenchmarkStorageHset-4  	 3000000	       385 ns/op
    BenchmarkStorageHget-4  	 5000000	       343 ns/op
    BenchmarkStorageExpire-4	 3000000	       493 ns/op
    BenchmarkStorageTtl-4   	 2000000	       585 ns/op
