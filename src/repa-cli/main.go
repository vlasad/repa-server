package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"regexp"
	"strings"
)

var conn net.Conn

func main() {
	// user can define ip and port of server to connect if it is not defined than default 127.0.0.1:3377
	server_port := flag.Int("p", 3377, "the port to connect, default: 3377")
	server_ip := flag.String("ip", "127.0.0.1", "the ip to connect, default: 127.0.0.1")
	flag.Parse()

	var err error
	// connecting to the server if error present than exit and print error
	conn, err = net.Dial("tcp", fmt.Sprintf("%v:%v", *server_ip, *server_port))
	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	// starting accept user's input
	startConsole()
}

func startConsole() {
	reader := bufio.NewReader(os.Stdin)

	// infinite loop in which we read command from user
	// example: set var value
	for {
		fmt.Printf("%v> ", conn.RemoteAddr())
		command, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		executeCommand(command)
	}
}

func executeCommand(command string) {
	params, err := parseCommand(command)
	if err != nil {
		fmt.Println(err)
	} else {
		sendRequest(params)
	}
}

// parse user's command to items and check correctness of them
func parseCommand(command string) (map[string]string, error) {
	command = strings.Trim(command, " \t\r\n")
	if command == "" {
		return map[string]string{}, nil
	}

	by_spaces := regexp.MustCompile("[\\s]+")
	items := by_spaces.Split(command, 2)

	params := make(map[string]string)
	params["command"] = items[0]

	switch items[0] {
	case "set", "push", "expire":
		items := by_spaces.Split(command, 3)
		if len(items) == 3 {
			params["key"] = items[1]
			params["value"] = items[2]
		} else {
			return params, errors.New("(error) syntax error")
		}
	case "get", "del", "pop", "ttl":
		items := by_spaces.Split(command, 2)
		if len(items) == 2 {
			params["key"] = items[1]
		} else {
			return params, errors.New("(error) syntax error")
		}
	case "hset":
		items := by_spaces.Split(command, 4)
		if len(items) == 4 {
			params["key"] = items[1]
			params["field"] = items[2]
			params["value"] = items[3]
		} else {
			return params, errors.New("(error) syntax error")
		}
	case "hget":
		items := by_spaces.Split(command, 3)
		if len(items) == 3 {
			params["key"] = items[1]
			params["field"] = items[2]
		} else {
			return params, errors.New("(error) syntax error")
		}
	case "keys":
	case "auth":
		items := by_spaces.Split(command, 2)
		if len(items) == 2 {
			params["value"] = items[1]
		} else {
			return params, errors.New("(error) syntax error")
		}
	default:
		return params, errors.New(fmt.Sprintf("(error) Unknown command '%s'", items[0]))
	}

	return params, nil
}

// parsed params are wrapped in server protocol format and send request to server
func sendRequest(params map[string]string) {
	if len(params) > 0 {
		request := ""
		for k, v := range params {
			request += fmt.Sprintf("%s: %s\r\n", k, v)
		}
		request += "\r\n"

		_, err := fmt.Fprintf(conn, request)
		if err != nil {
			log.Fatal(err)
		}

		readResponse()
	}
}

func readResponse() {
	var request bytes.Buffer
	reader := bufio.NewReader(conn)

	// read response until the end of it (\r\n\r\n)
	for !strings.HasSuffix(request.String(), "\r\n\r\n") {
		line, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		request.WriteString(line)
	}

	response := parseResponse(request.String())
	if v, ok := response["error"]; ok {
		fmt.Printf("(error) %s\n", v)
	} else {
		fmt.Println(response["result"])
	}
}

func parseResponse(raw string) map[string]string {
	lines := strings.SplitN(raw[:len(raw)-4], "\r\n", -1)

	r := make(map[string]string)
	for _, line := range lines {
		parts := strings.SplitN(line, ": ", 2)
		r[parts[0]] = parts[1]
	}

	return r
}
