package main

import (
	"flag"
)

var password = flag.String("pwd", "", "the server password")

func main() {
	port := flag.Int("p", 3377, "the port on which server start, default: 3377")
	master := flag.String("master", "", "the master ip:port, default: empty")
	flag.Parse()

	server := NewServer(*port, *master, true)
	server.Start()
}
