package main

import (
	"encoding/gob"
	"os"
	"time"
)

type DiskStorage struct {
	every    time.Duration
	filename string
	data     *map[string]StorageType
}

func EnableDiskStorage(every time.Duration, filename string, data *map[string]StorageType) *DiskStorage {
	st := &DiskStorage{
		every:    every,
		filename: filename,
		data:     data,
	}

	initGOB()
	st.load_from_disk()
	go st.startSaver()
	return st
}

func initGOB() {
	gob.Register(map[string]StorageType{})
	gob.Register(String{})
	gob.Register(List{})
	gob.Register(Hash{})
}

func (self *DiskStorage) startSaver() {
	for {
		self.save_to_disk()
		time.Sleep(self.every)
	}
}

func (self *DiskStorage) save_to_disk() {
	tmp_filename := "tmp_" + self.filename
	f, err := os.Create(tmp_filename)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	enc := gob.NewEncoder(f)
	if err := enc.Encode(self.data); err != nil {
		panic(err)
	}

	f.Close()

	if err := os.Rename(tmp_filename, self.filename); err != nil {
		panic(err)
	}
}

func (self *DiskStorage) load_from_disk() {
	f, err := os.Open(self.filename)
	if err != nil {
		return
	}
	defer f.Close()

	enc := gob.NewDecoder(f)
	if err := enc.Decode(&self.data); err != nil {
		panic(err)
	}
}
