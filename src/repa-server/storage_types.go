package main

type StorageType interface {
	isStorageType() bool
}

type String struct {
	Value     *string
	ExpiresAt int64
}

func (self String) isStorageType() bool {
	return true
}

type List struct {
	Value     *[]string
	ExpiresAt int64
}

func (self List) isStorageType() bool {
	return true
}

type Hash struct {
	Value     *map[string]string
	ExpiresAt int64
}

func (self Hash) isStorageType() bool {
	return true
}
