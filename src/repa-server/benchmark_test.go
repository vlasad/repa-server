package main

import (
	"fmt"
	"net"
	"os"
	"testing"
	"time"
)

var conn net.Conn
var storage *MemoryStorage

func TestMain(m *testing.M) {
	storage = NewMemoryStorage(false)

	server := NewServer(4455, "", false)
	go server.Start()

	time.Sleep(1 * time.Second)

	var err error
	conn, err = net.Dial("tcp", "127.0.0.1:4455")
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	os.Exit(m.Run())
}

func benchmarkServer(request string, b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := fmt.Fprint(conn, request)
		if err != nil {
			panic(err)
		}

		buf := make([]byte, 32)
		conn.Read(buf)
	}
}

func BenchmarkServerSet(b *testing.B) {
	benchmarkServer("command: set\r\nkey: k\r\nvalue: 123\r\n\r\n", b)
}
func BenchmarkServerGet(b *testing.B) {
	benchmarkServer("command: get\r\nkey: k\r\n\r\n", b)
}
func BenchmarkServerKeys(b *testing.B) {
	benchmarkServer("command: keys\r\n\r\n", b)
}
func BenchmarkServerPush(b *testing.B) {
	benchmarkServer("command: push\r\nkey: arr\r\nvalue: 123\r\n\r\n", b)
}
func BenchmarkServerPop(b *testing.B) {
	benchmarkServer("command: pop\r\nkey: arr\r\n\r\n", b)
}
func BenchmarkServerHset(b *testing.B) {
	benchmarkServer("command: hset\r\nkey: hash\r\nfield: f\r\nvalue: 123\r\n\r\n", b)
}
func BenchmarkServerHget(b *testing.B) {
	benchmarkServer("command: hget\r\nkey: hash\r\nfield: f\r\n\r\n", b)
}
func BenchmarkServerExpire(b *testing.B) {
	benchmarkServer("command: expire\r\nkey: k\r\nvalue: 123\r\n\r\n", b)
}
func BenchmarkServerTtl(b *testing.B) {
	benchmarkServer("command: ttl\r\nkey: k\r\n\r\n", b)
}

func benchmarkStorage(request map[string]string, b *testing.B) {
	for n := 0; n < b.N; n++ {
		storage.Execute(request)
	}
}

func BenchmarkStorageSet(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "set"
	r["key"] = "k"
	r["value"] = "123"
	benchmarkStorage(r, b)
}
func BenchmarkStorageGet(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "get"
	r["key"] = "k"
	benchmarkStorage(r, b)
}
func BenchmarkStorageKeys(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "keys"
	benchmarkStorage(r, b)
}
func BenchmarkStoragePush(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "push"
	r["key"] = "arr"
	r["value"] = "123"
	benchmarkStorage(r, b)
}
func BenchmarkStoragePop(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "pop"
	r["key"] = "arr"
	benchmarkStorage(r, b)
}
func BenchmarkStorageHset(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "hset"
	r["key"] = "arr"
	r["field"] = "f"
	r["value"] = "123"
	benchmarkStorage(r, b)
}
func BenchmarkStorageHget(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "hget"
	r["key"] = "arr"
	r["field"] = "f"
	benchmarkStorage(r, b)
}
func BenchmarkStorageExpire(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "expire"
	r["key"] = "arr"
	r["value"] = "123"
	benchmarkStorage(r, b)
}
func BenchmarkStorageTtl(b *testing.B) {
	r := make(map[string]string)
	r["command"] = "ttl"
	r["key"] = "arr"
	benchmarkStorage(r, b)
}
