package main

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"
)

type MemoryStorage struct {
	sync.RWMutex
	m map[string]StorageType
}

func NewMemoryStorage(isSaveToDisk bool) *MemoryStorage {
	st := &MemoryStorage{
		m: make(map[string]StorageType),
	}

	go st.removeAllExpiredItems()
	if isSaveToDisk {
		EnableDiskStorage(1*time.Minute, "repa.rdb", &st.m)
	}

	return st
}

func (self *MemoryStorage) Execute(request map[string]string) (result string, err error) {
	self.removeIfExpired(request["key"])

	switch request["command"] {
	case "set":
		result, err = self.set(request["key"], request["value"])
	case "get":
		result, err = self.get(request["key"])
	case "del":
		result = self.del(request["key"])
	case "keys":
		result = self.keys()
	case "push":
		result, err = self.push(request["key"], request["value"])
	case "pop":
		result, err = self.pop(request["key"])
	case "hset":
		result, err = self.hset(request["key"], request["field"], request["value"])
	case "hget":
		result, err = self.hget(request["key"], request["field"])
	case "expire":
		i, _ := strconv.ParseInt(request["value"], 10, 64)
		result, err = self.expire(request["key"], i)
	case "ttl":
		result, err = self.ttl(request["key"])
	case "sync":
		result, err = self.sync()
	case "auth":
		result, err = self.auth(request["value"])
	default:
		err = errors.New(fmt.Sprintf("Unknown command '%s'", request["command"]))
	}

	return
}

func (self *MemoryStorage) set(key, value string) (string, error) {
	self.Lock()
	defer self.Unlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case String:
			v.Value = &value
			self.m[key] = v
			return "0", nil
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		self.m[key] = String{Value: &value}
		return "1", nil
	}
}

func (self *MemoryStorage) get(key string) (string, error) {
	self.RLock()
	defer self.RUnlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case String:
			return *v.Value, nil
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		return "", errors.New("The key is not exists")
	}
}

func (self *MemoryStorage) del(key string) string {
	self.Lock()
	defer self.Unlock()

	if _, ok := self.m[key]; ok {
		delete(self.m, key)
		return "1"
	}
	return "0"
}

func (self *MemoryStorage) keys() string {
	self.RLock()
	defer self.RUnlock()

	keys := make([]string, 0, len(self.m))
	for k := range self.m {
		keys = append(keys, k)
	}
	return strings.Join(keys, ", ")
}

func (self *MemoryStorage) push(key, value string) (string, error) {
	self.Lock()
	defer self.Unlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case List:
			*v.Value = append(*v.Value, value)
			self.m[key] = v
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		self.m[key] = List{Value: &[]string{value}}
	}

	v := self.m[key].(List)
	return fmt.Sprintf("%v", len(*v.Value)), nil
}

func (self *MemoryStorage) pop(key string) (string, error) {
	self.RLock()
	defer self.RUnlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case List:
			if len(*v.Value) > 0 {
				x := (*v.Value)[len(*v.Value)-1]
				*v.Value = (*v.Value)[:len(*v.Value)-1]
				self.m[key] = v
				return x, nil
			} else {
				return "", errors.New("The list is empty")
			}
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		return "", errors.New("The key is not exists")
	}
}

func (self *MemoryStorage) hset(key, field, value string) (string, error) {
	self.Lock()
	defer self.Unlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case Hash:
			if _, ok := (*v.Value)[field]; ok {
				(*v.Value)[field] = value
				return "0", nil
			} else {
				(*v.Value)[field] = value
				return "1", nil
			}
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		self.m[key] = Hash{Value: &map[string]string{field: value}}
		return "1", nil
	}
}

func (self *MemoryStorage) hget(key, field string) (string, error) {
	self.RLock()
	defer self.RUnlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case Hash:
			if value, ok := (*v.Value)[field]; ok {
				return value, nil
			} else {
				return "", errors.New("The field is not exists")
			}
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}
	} else {
		return "", errors.New("The key is not exists")
	}
}

func (self *MemoryStorage) expire(key string, seconds int64) (string, error) {
	self.Lock()
	defer self.Unlock()

	if _, ok := self.m[key]; ok {
		switch v := self.m[key].(type) {
		case String:
			v.ExpiresAt = time.Now().Unix() + seconds
			self.m[key] = v
		case List:
			v.ExpiresAt = time.Now().Unix() + seconds
			self.m[key] = v
		case Hash:
			v.ExpiresAt = time.Now().Unix() + seconds
			self.m[key] = v
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}

		return "OK", nil
	} else {
		return "", errors.New("The key is not exists")
	}
}

func (self *MemoryStorage) ttl(key string) (string, error) {
	self.RLock()
	defer self.RUnlock()

	if _, ok := self.m[key]; ok {
		var seconds int64
		switch v := self.m[key].(type) {
		case String:
			seconds = v.ExpiresAt
		case List:
			seconds = v.ExpiresAt
		case Hash:
			seconds = v.ExpiresAt
		default:
			return "", errors.New("Operation against a key holding the wrong kind of value")
		}

		if seconds != 0 {
			return fmt.Sprintf("%v", seconds-time.Now().Unix()), nil
		} else {
			return "-1", nil
		}
	} else {
		return "", errors.New("The key is not exists")
	}
}

func (self *MemoryStorage) removeIfExpired(key string) {
	if self.isExpired(key) {
		self.Lock()
		if self.isExpired(key) {
			delete(self.m, key)
		}
		self.Unlock()
	}
}

func (self *MemoryStorage) isExpired(key string) bool {
	if _, ok := self.m[key]; ok {
		var seconds int64
		switch v := self.m[key].(type) {
		case String:
			seconds = v.ExpiresAt
		case List:
			seconds = v.ExpiresAt
		case Hash:
			seconds = v.ExpiresAt
		}

		if seconds > 0 && seconds < time.Now().Unix() {
			return true
		}
	}

	return false
}

func (self *MemoryStorage) removeAllExpiredItems() {
	for {
		for k := range self.m {
			self.removeIfExpired(k)
		}

		time.Sleep(5 * time.Minute)
	}
}

func (self *MemoryStorage) sync() (string, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	if err := enc.Encode(&self.m); err != nil {
		fmt.Println(err)
	}

	str := b.String()
	return fmt.Sprintf("%d#%s", len(str), str), nil
}

func (self *MemoryStorage) auth(value string) (string, error) {
	if *password != "" && value != *password {
		return "", errors.New("The password is invalid")
	}

	return "OK", nil
}
