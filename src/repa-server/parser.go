package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func ParseRequest(raw string) (map[string]string, error) {
	lines := strings.SplitN(raw[:len(raw)-4], "\r\n", -1)

	r := make(map[string]string)
	for _, line := range lines {
		parts := strings.SplitN(line, ": ", 2)

		if len(parts) != 2 {
			return nil, errors.New("Bad request's format")
		}

		switch parts[0] {
		case "command", "key", "field", "value":
			r[parts[0]] = parts[1]
		default:
			return nil, errors.New("Bad request's format")
		}
	}

	if err := validateRequest(r); err != nil {
		return nil, err
	}

	return r, nil
}

var rules = map[string]map[string][]string{
	"set": {
		"required": {"key", "value"},
	},
	"get": {
		"required": {"key"},
	},
	"del": {
		"required": {"key"},
	},
	"keys": {},
	"push": {
		"required": {"key", "value"},
	},
	"pop": {
		"required": {"key"},
	},
	"hset": {
		"required": {"key", "field", "value"},
	},
	"hget": {
		"required": {"key", "field"},
	},
	"expire": {
		"required": {"key", "value"},
		"integer":  {"value"},
	},
	"ttl": {
		"required": {"key"},
	},
	"sync": {},
	"auth": {
		"required": {"value"},
	},
}

func validateRequest(r map[string]string) error {
	if _, ok := rules[r["command"]]; !ok {
		return errors.New(fmt.Sprintf("Unknown command '%s'", r["command"]))
	}

	if required, ok := rules[r["command"]]["required"]; ok {
		for _, name := range required {
			if _, ok := r[name]; !ok {
				return errors.New(fmt.Sprintf("'%s' is required", name))
			}
		}
	}

	if integer, ok := rules[r["command"]]["integer"]; ok {
		for _, name := range integer {
			if _, err := strconv.ParseInt(r[name], 10, 64); err != nil {
				return errors.New(fmt.Sprintf("'%s' must be integer", name))
			}
		}
	}

	return nil
}
