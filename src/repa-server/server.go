package main

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

type Server struct {
	port    int
	master  string
	storage *MemoryStorage
}

func NewServer(port int, master string, isSaveToDisk bool) *Server {
	s := &Server{
		port:    port,
		master:  master,
		storage: NewMemoryStorage(isSaveToDisk),
	}

	return s
}

func (self *Server) Start() {
	go self.syncLoop(30 * time.Second)

	l, err := net.Listen("tcp", fmt.Sprintf(":%v", self.port))
	if err != nil {
		log.Fatal(err)
	}

	defer l.Close()

	// accept connections in infinite loop and handle every connection in it's own goroutine
	for {
		c, err := l.Accept()
		if err != nil {
			log.Println(err)
		}

		go connectionHandler(Connection{conn: c}, self.storage)
	}
}

type Connection struct {
	conn   net.Conn
	isAuth bool
}

func connectionHandler(c Connection, s *MemoryStorage) {
	defer c.conn.Close()

	for {
		raw_request, err := readRequest(c.conn)
		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			return
		}

		var response string
		var r_error error

		request, err := ParseRequest(raw_request)
		if *password != "" && !c.isAuth && request["command"] != "auth" && request["command"] != "sync" {
			r_error = errors.New("Authentication is required")
		} else {
			if err != nil {
				r_error = err
			} else {
				response, r_error = s.Execute(request)
				if request["command"] == "auth" && r_error == nil {
					c.isAuth = true
				}
			}
		}

		err = writeResponse(c.conn, response, r_error)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func readRequest(conn net.Conn) (string, error) {
	var request bytes.Buffer
	reader := bufio.NewReader(conn)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			return "", err
		}

		request.WriteString(line)

		if strings.HasSuffix(request.String(), "\r\n\r\n") {
			return request.String(), nil
		}
	}
}

func writeResponse(conn net.Conn, response string, r_error error) error {
	var buffer bytes.Buffer

	if r_error != nil {
		buffer.WriteString(fmt.Sprintf("error: %s\r\n", r_error))
	} else {
		buffer.WriteString(fmt.Sprintf("result: %s\r\n", response))
	}

	buffer.WriteString("\r\n")

	_, err := fmt.Fprint(conn, buffer.String())
	if err != nil {
		return err
	}

	return nil
}

func (self *Server) syncLoop(every time.Duration) {
	if self.master != "" {
		for {
			self.sync_once()
			time.Sleep(every)
		}
	}
}

func (self *Server) sync_once() {
	conn, err := net.Dial("tcp", self.master)
	if err != nil {
		fmt.Printf("Master server is not available:\n%v\n", err)
		return
	}
	defer conn.Close()

	_, err = fmt.Fprint(conn, "command: sync\r\n\r\n")
	if err != nil {
		fmt.Println(err)
		return
	}

	reader := bufio.NewReader(conn)
	line, err := reader.ReadString('#')
	if err != nil {
		fmt.Println(err)
		return
	}

	parts := strings.SplitN(line, ": ", 2)
	body_length, err := strconv.ParseInt(parts[1][:len(parts[1])-1], 10, 64)
	if err != nil {
		fmt.Println(err)
		return
	}

	if body_length > 0 {
		buffer := make([]byte, body_length)
		n, err := reader.Read(buffer)
		if err != nil {
			fmt.Println(err)
			return
		}
		if int64(n) != body_length {
			fmt.Println("body is corrupted")
			return
		}

		enc := gob.NewDecoder(bytes.NewReader(buffer))
		if err := enc.Decode(&self.storage.m); err != nil {
			fmt.Println(err)
			return
		}
	}
}
